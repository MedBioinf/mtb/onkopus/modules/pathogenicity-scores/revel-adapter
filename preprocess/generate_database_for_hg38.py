#! /usr/bin/env python3

import argparse
import sys
from io import BytesIO
import json
import gzip


### Some config parameter
sourceVersion='hg19'
targetVersion='hg38'

infile = None
outfile = None

def parse_args():
    global infile, outfile
    parser = argparse.ArgumentParser("VCF parser for skipping empty hg38 lines")
    parser.add_argument('infile', nargs='?', type=argparse.FileType('r'), default='-', help="Path to the (gzipped) input file, or stdin if not given")
    parser.add_argument('outfile', nargs='?', type=argparse.FileType('w'), default=sys.stdout, help="Path to the output file, or stdout if not given")

    args = parser.parse_args()
    infile = args.infile
    outfile = args.outfile

if __name__=="__main__":
    parse_args()

    file = infile
    if infile.name.endswith('.gz'):
        file = gzip.open(infile.name, 'rt')


    for line in file:
        if line.startswith('chr'):
            print('skip description line')
            continue
        fields = line.split('\t')
        chr,hg19pos, hg38pos, refBase, altBase,aaref, aaalt,revel, ensembleid = fields[0], fields[1], fields[2], fields[3], fields[4],fields[5],fields[6],fields[7],fields[8]
        if hg38pos == '.':
            print('point line {}'.format(line.strip()))
            continue
        print("\t".join(fields).strip(), file=outfile)
