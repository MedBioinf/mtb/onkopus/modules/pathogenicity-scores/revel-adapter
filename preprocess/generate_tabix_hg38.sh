
#python3 generate_database_for_hg38.py ./revel_with_transcript_ids.tsv.gz ./revel_with_transcript_ids_hg38.tsv
zgrep --color -v "^[0-9XY]*[[:space:]]*[0-9]*[[:space:]]*\." revel_with_transcript_ids.tsv.gz | gzip > revel_with_transcript_ids_hg38.tsv.gz

#cat revel_with_transcript_ids_hg38.tsv | awk '$1 ~ /^#/ {print $0;next} {print $0 | "sort -k1,1V -k3,3n"}' > revel_with_transcript_ids_hg38_sorted.tsv
cat revel_with_transcript_ids_hg38.tsv | sort -k1,3 -n > revel_with_transcript_ids_hg38_sorted.tsv
bgzip -c revel_with_transcript_ids_hg38_sorted.tsv > revel_with_transcript_ids_hg38_sorted.tsv.gz

tabix -s1 -b3 -e3 ./revel_with_transcript_ids_hg38_sorted.tsv.gz
