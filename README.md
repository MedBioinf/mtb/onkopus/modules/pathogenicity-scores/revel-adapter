# REVEL Adapter

This microservice returns the REVEL - Score of a variant. It currently uses a file named `revel_with_transcript_ids.tsv.gz`.

The file can be downloaded from https://sites.google.com/site/revelgenomics/downloads and processed using the script `process_revel_file.sh`.

It is a very early version (WIP).

More information about REVEL can be found [here](https://www.cell.com/ajhg/fulltext/S0002-9297(16)30370-6).

## Requirements <a name="requirements"></a>

If a docker-image is already provided, only [Docker](https://docs.docker.com/get-docker/) and docker-compose need to be installed. 

Otherwise [Maven](https://maven.apache.org/users/index.html) needs to be installed in addition to that.


## General Setup

1. Clone the git project
2. Install and set up the [Requirements](#requirements)

### If the docker image still needs to be build:

1. Run `mvnw package`, for example with:

```
./mvnw clean package
```

2. Create the docker image(or `tag` a container as `reveladapter:latest`):

```
docker build -f src/main/docker/Dockerfile.jvm -t reveladapter .
```


### With working docker images:

3. Run `docker-compose up`

4. Query the program with the wanted genomic positions (+variant) (see below)


## Querying the service

The service accepts variants in the format `chr:positionREF>VAR`.

It will return some JSON in a preliminary format.

## Endpoints

Currently, there are two endpoints:

- `revel/v1/full?genompos={variant}`: Delivers JSON output of information provided by REVEL for a variant with sequence position on hg19 (GRCh37)

- `revel/v1/info/`: Shows some basic information regarding the tool.

Example: `curl -X GET "http://localhost:8096/revel/v1/full?genompos=chr1:35145C>A" | jq`

## Swagger UI

`revel/v1/doc/`

### What if Tabix not working?

Problems to look for:

1. Tabix only allows TAB separator
2. use cmd arguments to define the columns to be used

```
-s1 -> first col for chromosome
-b2 -> second col for begin pos
-e2 -> if no end pos is given, use begin pos
```

3. ignoring header sometimes works (especially in combination with above arguments)

```
-S1 -> ignore first line
```

3. unzip and bgzip the file again

