import logging
import conf.read_config as conf_reader


def setup_custom_logger(name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(levelname)s - %(module)s - %(message)s')

    handler = logging.StreamHandler()
    handler.setFormatter(formatter)

    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)
    logger.addHandler(handler)

    try:
        fileHandler = logging.FileHandler("{0}/{1}.log".format(conf_reader.__LOG_PATH__, conf_reader.__LOG_FILE__))
        fileHandler.setFormatter(formatter)
        logger.addHandler(fileHandler)
    except:
        print("Could not generate log file handler")

    return logger
