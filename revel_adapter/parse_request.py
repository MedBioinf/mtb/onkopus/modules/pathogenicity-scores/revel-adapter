import traceback, re, subprocess
import conf.read_config as conf_reader
import logging


def generate_tabix_query(variant_list):
    """


    :param variant_list:
    :return:
    """
    response = {}
    genome_pos_exp = "(chr)([0-9|X|Y]+):(g\.)?([0-9]+)([A|G|C|T|N]+)>([A|G|C|T|N]+)"
    p = re.compile(genome_pos_exp)

    tabix_q = ''
    for variant in variant_list:
        response[variant] = {}

        try:
            groups = p.match(variant)
            chrom = groups.group(2)
            pos = groups.group(4)
            #ref = groups.group(4)
            #alt = groups.group(5)
            #print(groups.groups())

            #tabix_q += chrom + ":" + pos + "-" + pos + " "
            tabix_q += get_tabix_positions(chrom, pos)
        except:
            logger = logging.getLogger('root')
            logger.debug("Could not parse genomic location: ",variant,traceback.format_exc())

    return tabix_q


def get_revel_entry(variant_list, genome_version):
    """
    Retrieves the ClinVar entries for a list of genomic locations

    :param variant_list:
    :param genome_version:
    :return:
    """
    hg19 = False
    db_file = conf_reader.__DATA_PATH__ + "/" + conf_reader.__DATABASE_FILE__
    if genome_version=="hg19":
        hg19 = True

    tabix_q = generate_tabix_query(variant_list)

    tabix_query = "tabix " + db_file + " "+ tabix_q
    print(tabix_query)

    process = subprocess.Popen(tabix_query.split(), stdout=subprocess.PIPE)
    output, error = process.communicate()
    output = output.decode("utf-8")

    results = output.split('\n')
    response = {}
    try:
        for result in results:
            #print(result)
            elements = result.split("\t")
            if len(elements) > 1:
                chrom_res = elements[0]
                pos_res = elements[2]
                ref_res = elements[3]
                alt_res = elements[4]
                var = "chr"+chrom_res + ":" + pos_res + ref_res + ">" + alt_res
                if var in variant_list:
                    if var not in response:
                        response[var] = {}
                    response[var][conf_reader.config["DEFAULT"]["MODULE_PREFIX"]] = generate_json_obj_from_database_array(elements)
                else:
                    print("variant not found: ",var,": ",variant_list)
                    pass
    except:
        print(traceback.format_exc())
    return response


def generate_json_obj_from_database_array(elements):
    """
    Generates a JSON dictionary from a dbNSFP database entry array

    :param elements:
    :return:
    """
    json_obj = {}

    score = elements[7]
    json_obj["Score"] = score
    json_obj["ensembl_id"] = elements[8]
    json_obj["q_id"] = "chr"+ elements[0] + ":"  + elements[2] + elements[3] + ">" + elements[4]
    json_obj["hg19_pos"] = elements[1]
    json_obj["hg38_pos"] = elements[2]

    return json_obj

def get_tabix_positions(chrom, pos):
    """
    Retrieves the NCBI chromosome identifiers and adds a tabix query for each chromosome identifier

    :param chrom:
    :param pos:
    :return:
    """
    q=""
    #chrom_list = _get_chromosome_accessor(chrom)
    chrom_list = chrom
    q += chrom+ ":" + pos + "-" + pos + " "
    #for ncbi_chrom in chrom_list:
    #    q+= ncbi_chrom+ ":" + pos + "-" + pos + " "
    #q = q.rstrip(" ")
    return q
