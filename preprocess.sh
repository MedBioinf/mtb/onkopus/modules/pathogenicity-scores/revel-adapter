#!/usr/bin/bash

# downloads and adjusts REVEL file
REVEL="revel-v1.3_all_chromosomes"
DB_FILE="revel_with_transcript_ids.tsv.gz"

# Check if database files are present, download them if they cannot be found
if [ ! -f "${DATA_PATH}/${DB_FILE}" ]
then
  echo "Could not file database file ${DATA_PATH}/${DB_FILE}. Downloading file..."
  cd ${DATA_PATH}
  wget -v "https://rothsj06.dmz.hpc.mssm.edu/revel-v1.3_all_chromosomes.zip"
  unzip ${REVEL}.zip

  sed 's/,/\t/g' revel_with_transcript_ids > revel_with_transcript_ids.tsv
  bgzip revel_with_transcript_ids.tsv

  zgrep --color -v "^[0-9XY]*[[:space:]]*[0-9]*[[:space:]]*\." revel_with_transcript_ids.tsv.gz > revel_with_transcript_ids_hg38.tsv
  tail -n +2 revel_with_transcript_ids_hg38.tsv > revel_with_transcript_ids_hg38.tsv.no_header

  cat revel_with_transcript_ids_hg38.tsv.no_header | sort -k1,1 -k3,3n > revel_with_transcript_ids_hg38_sorted.tsv
  sed -i '1 i chr\thg19_pos\tgrch38_pos\tref\talt\taaref\taaalt\tREVEL\tEnsembl_transcriptid' revel_with_transcript_ids_hg38_sorted.tsv

  bgzip -c revel_with_transcript_ids_hg38_sorted.tsv > revel_with_transcript_ids_hg38_sorted.tsv.gz
  tabix -S1 -s1 -b3 -e3 ./revel_with_transcript_ids_hg38_sorted.tsv.gz
  tabix -S1 -s1 -b2 -e2 ./revel_with_transcript_ids.tsv.gz
fi

/opt/venv/bin/python3 /app/app.py
