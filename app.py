from flask import request
from flask_cors import CORS
from flask import Flask
from flask_swagger_ui import get_swaggerui_blueprint
import conf.read_config as conf_reader
import revel_adapter
from revel_adapter.tools import log

DEBUG = True
SERVICE_NAME="revel"
VERSION="v1"

app = Flask(__name__)
app.config.from_object(__name__)

CORS(app, resources={r'/*': { 'origins': '*' }})
SWAGGER_URL = f'/{SERVICE_NAME}/{VERSION}/docs'
API_URL = '/static/config.json'

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "REVEL-Adapter"
    },
)

# definitions
SITE = {
        'logo': 'FLASK-VUE',
        'version': '0.0.1'
}

logger = log.setup_custom_logger('root')
logger.debug('Starting up Flask service...')

@app.route(f'/{SERVICE_NAME}/{VERSION}/<genome_version>/full', methods=['GET'])
def get_score(genome_version=None):
    print("revel")
    variant = request.args.get("genompos")
    variants= variant.split(",")
    response = revel_adapter.get_revel_entry(variants, genome_version)
    return response


if __name__ == '__main__':
    app.register_blueprint(swaggerui_blueprint)
    app.run(host='0.0.0.0', debug=True, port=conf_reader.__PORT__)
